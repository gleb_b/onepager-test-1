/* vertical lines height calc */
function linesHeightCalc() { 
	var linesHeight = $(window).height()*.45 - $('.hero__card').height()/2 - 120 + 'px';
	$('.hero__vertical-lines').css('height', linesHeight);
	}	


function loadAjax(i) {
       $('.popup__number').text(i); 
       $('.popup__details').load('https://gleb_b.gitlab.io/onepager-test-1/popup-'+i+'.html');    
}	

function popup(i) {
	loadAjax(i);
	$('.popup').addClass('popup_active');
}

function closePopup() {
	$('.popup').removeClass('popup_active');
}

$(document).ready(function(){
	 linesHeightCalc();

	 /* Search Input*/
	 $('.header__toggle').click(function(){
	 	if (!$('.header__menu').hasClass('header__menu_active')) { $('.header__menu').addClass('header__menu_active')}
	 	else { $('.header__menu').removeClass('header__menu_active') }
	 })
});

$( window ).resize(function() {
  linesHeightCalc();
});


$(window).scroll(function(){
  if($(document).scrollTop() > 0)
	{
  		$('.header').addClass('header_active');
  		$('.header__container').addClass('header__container_active');
    }
	else
  	{
   		$('.header').removeClass('header_active');
   		$('.header__container').removeClass('header__container_active');
    }
});