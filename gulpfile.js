var gulp = require('gulp'),
	sass = require('gulp-sass'),  
	cssNano = require ('gulp-cssnano'),  
	autoPrefix = require ('gulp-autoprefixer'),  
	uglifyJS = require ('gulp-uglifyjs');  

gulp.task ('sassCompile' , function() {
	return gulp.src('app/sass/*.sass')
	.pipe(sass().on('error', sass.logError))
	.pipe(gulp.dest('app/css'))	;
})

gulp.task ('css', ['sassCompile'], function() {
	return gulp.src('app/css/*.css')
	.pipe(autoPrefix(['last 15 versions']))
	.pipe(cssNano({zindex: false}))
	.pipe(gulp.dest('public/css'))	;
})

gulp.task ('js', function() {
	return gulp.src('app/js/*.js')
	.pipe(uglifyJS())
	.pipe(gulp.dest('public/js'))	;
})

gulp.task ('build', ['css', 'js'], function() {	
	return gulp.src('app/img/*.jpg')
	.pipe(gulp.dest('public/img')) ;
	return gulp.src('app/*.html')
	.pipe(gulp.dest('public')) ;
} )